# ------------
# diplomacy_read
# ------------

def diplomacy_read(s):
    """
    read three elements
    s a string
    return a list of elements, representing an army, location and action
    """
    a = s.split()
    if len(a) == 4:
        return [a[0], a[1], a[2], a[3]]
    else:
        return [a[0], a[1], a[2]]


# ------------
# diplomacy_eval
# ------------

def diplomacy_eval(inpt):
    """
    compute the status of various armies after they make 1 round of moves. 
    make assertions that check pre-conditions, post-conditions, argument validity, and return-value validity
    """
    assert len(inpt) >= 3

    for element in inpt:    # initialize locations
        armies_dict[element[0]] = element[1]

    for e in inpt:              #check move intentions for all armies and set in dest_dict
        if e[2] == "Move":
            dest_dict[e[0]] = e[3]
            armies_dict[e[0]] = e[3]
        else:
            dest_dict[e[0]] = e[1]

    # check for support
    for element in inpt:
        if element[2] == 'Support':
            elem_loc = element[1]
            attackedflag = False
            for other_elem in inpt:
                if other_elem[2] == 'Move' and other_elem[3] == elem_loc: #if another army is moving / support is invalid
                    attackedflag = True    #do NOT add support bonus
            if not attackedflag:
                support_dict[element[3]] += 1   #add support bonus to B
    
    #test for armies in combat
    for army1, dest1 in dest_dict.items():       
        for army2, dest2 in dest_dict.items():
            if army2 != army1 and dest1 == dest2:
                combat_dict[army1] = True
                combat_dict[army2] = True
                
    for army1 in combat_dict.keys():
        for army2 in combat_dict.keys():
            if army2 != army1 and support_dict[army1] > support_dict[army2]:
                armies_dict[army2] = "[dead]"
            if army2 != army1 and support_dict[army1] == support_dict[army2]:
                armies_dict[army2] = "[dead]"
                armies_dict[army1] = "[dead]"
            if army2 != army1 and support_dict[army1] < support_dict[army2]:
                armies_dict[army1] = "[dead]"

    #return a list of armies (sorted alphabetically) and their locations
    final_lst = []
    for a, final_loc in armies_dict.items():
        final_lst.append(a + ' ' + final_loc)
    assert len(final_lst) == len(inpt)
    return sorted(final_lst)


# -------------
# diplomacy_solve
# -------------

def diplomacy_solve(r, w):
    """
    r a reader
    w a writer
    Calls the eval function where the heavy lifting is done and then returns the results and formats them accordingly
    """
    global armies_dict
    global support_dict
    global dest_dict
    global combat_dict
    combat_dict = {}
    armies_dict = {}
    support_dict = {}
    dest_dict = {}
    inpt_lst = []
    for s in r:
        splt_inpt = diplomacy_read(s) #splits it up
        support_dict[splt_inpt[0]] = 0   # initialize each army's support
        inpt_lst.append(splt_inpt)

    fin = diplomacy_eval(inpt_lst)
    for army in fin:
        w.write(army + "\n")
    return 
