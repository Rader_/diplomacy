from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval,  diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A NewYork Hold\n"
        i, j, k = diplomacy_read(s)
        self.assertEqual(i, 'A')
        self.assertEqual(j, 'NewYork')
        self.assertEqual(k, 'Hold')

    def test_read_2(self):
        s = "D Austin Support A\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i, 'D')
        self.assertEqual(j, 'Austin')
        self.assertEqual(k, 'Support')
        self.assertEqual(l, 'A')

    def test_read_3(self):
        s = "F Paris Move Rome\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i, 'F')
        self.assertEqual(j, 'Paris')
        self.assertEqual(k, 'Move')
        self.assertEqual(l, 'Rome')

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
    
    def test_solve_4(self):
        r = StringIO("B Barcelona Move Madrid\nC London Move Madrid\nA Madrid Hold\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")


# ----
# main
# ----
if __name__ == "__main__":
    main()


""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out

.......
----------------------------------------------------------------------
Ran 7 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          54      0     40      0   100%
TestDiplomacy.py      46      0      0      0   100%
--------------------------------------------------------------
TOTAL                100      0     40      0   100%

"""
